/**
 * package.json
 */
import 'jquery';
import 'popper.js';
import 'bootstrap';
import 'vanilla-cookieconsent';
//import 'footable'; // footable.min.js, footable.sort.min.js, footable.filter.min.js

/**
 * Babel core polyfill (e.g. "for of")
 */
import 'core-js/actual';

/**
 * External polyfill
 */
import cssVars from 'css-vars-ponyfill';
cssVars({});

/**
 * Custom scripts
 */
// import './_jquery.js';
import './_smooth-scrolling.js';
import '../modules/iframemanager/iframemanager.js';
import './_cookieconsent-init.js';
import './_global.js';
import './_one-page.js';
