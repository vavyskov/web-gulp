import smoothscroll from 'smoothscroll-polyfill';
smoothscroll.polyfill();

const links = document.querySelectorAll('a[href^="#"]');

// ES6 ("for of" does not work in IE11)
for (const link of links) {
    link.addEventListener('click', clickHandler);
}

// https://closure-compiler.appspot.com/ (Optimalization: Advanced)
// function a(){var d=b,e=0;return function(){return e<d.length?{done:!1,value:d[e++]}:{done:!0}}}var c,b=links,f="undefined"!=typeof Symbol&&Symbol.iterator&&b[Symbol.iterator];c=f?f.call(b):{next:a()};for(var g=c.next();!g.done;g=c.next())g.value.addEventListener("click",clickHandler);

// ES6 ("NodeList.prototype.forEach" does not work in IE11)
/* links.forEach(link => {
  link.addEventListener("click", clickHandler);
}); */
/* links.forEach(function (link) {
  link.addEventListener("click", clickHandler);
}); */

// ES5 ("for" works in IE11)
/* for (var i = 0; i < links.length; i++) {
  links[i].addEventListener("click", clickHandler);
} */

// https://closure-compiler.appspot.com/ (Optimalization: Advanced)
// for(var a=0;a<links.length;a++)links[a].addEventListener("click",clickHandler);

function clickHandler(e) {
    e.preventDefault();
    const href = this.getAttribute('href');

    // Optionally
    // const offsetCorrection = 100;
    const offsetCorrection = 0;

    /**
     * Window.scroll()
     */
    if (href !== '#') {
        const offsetTop = document.querySelector(href).offsetTop - offsetCorrection;
        window.scroll({
            top: offsetTop,
            behavior: 'smooth',
        });
    }

    /**
     * Element.scrollIntoView()
     */
    //   document.querySelector(href).scrollIntoView({
    //     behavior: "smooth"
    //   });
}

// scroll to bottom
const scrollToBottom = document.querySelector('.scroll-to-bottom');
if (scrollToBottom) {
    scrollToBottom.addEventListener('click', (e) => {
        e.preventDefault();
        // window.scrollTo({ top: document.body.clientHeight - window.innerHeight + 98, left: 0, behavior: 'smooth' });
        window.scrollTo({
            top: document.body.scrollHeight,
            behavior: 'smooth',
        });
    });
}
