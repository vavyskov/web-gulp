/**
 * Období
 */
 var period = document.getElementById('va-period');
 var currentYear = new Date().getFullYear();
 if (period) {
    period.innerHTML = ', 2009&ndash;' + currentYear;
 }

 // document.getElementById('period').innerHTML = ', 2009&ndash;' + new Date().getFullYear();




/**
 * Ukazatel posunu
 */
var scrollProgress = document.getElementById('va-scroll-progress');

function setScrollProgress() {
    var windowHeight = window.innerHeight;
    var documentHeight = document.body.clientHeight;
    var scrollPositionInPixels = window.scrollY;
    var scrollPositionInPercent = scrollPositionInPixels / (documentHeight - windowHeight) * 100;

    scrollProgress.style.width = scrollPositionInPercent + '%';

    //scrollProgress.style.width = (window.scrollY / (document.body.clientHeight - window.innerHeight) * 100) + '%';
}

window.addEventListener('scroll', setScrollProgress);

// První načtení (nastavení ukazatele bez posunu stránky)
setScrollProgress();
