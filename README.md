# WEB-gulp

Pomocný nástroj příkazového řádku pro vytváření statických webových stránek nabízející:
 - komprimace HTML, CSS a JS
 - preprocesor SCSS
 - autoprefixer
 - převod JavaScriptu verze ES6 do verze ES5
 - možnost u HTML souborů používat šablonovací systém EJS
 - komprese a generování náhledů obrázků
 - synchronizace prováděných změn s webovými prohlížeči (i různá zařízení)
 - synchronizace procházení webu ve více webových prohlížečích (i různá zařízení)

## Požadavky
1. Výsledný web bude funkční i bez webového serveru:
   - uložení Cookies vyžaduje webový server :( -> lokálně např. `npx http-server`
2. Cesty k souborům ve zdrojovém adresáři budou funkční a shodné s cestami ve výsledném adresáři (umožňuje v editoru využívat kontextovou nabídku při práci se soubory).

## Adresářová struktura

    docs/
        css/
    public/
    src/
        assets/
            audio/
            brand/
            download/
            favicon/
            image/
                resize/
            video/
        css/
        js/
        modules/
            bootstrap/
            ckeditor/
            cookieconsent/
            flipdown/
            fontawesome-free/
            footable/
            iframemanager/
            jquery/
            popper.js/
            sanitize.css/
            slick-carousel/
            vanilla-cookieconsent/

Popis adresářové struktury:

**docs** - dokumentace

**public** - výsledný zkomprimovaný web

**src** - zdrojové HTML soubory (mohou obsahovat EJS - Effective JavaScript Templating)

- **css** - CSS soubory s příponou .css, SCSS soubory s podtržítkem na začátku a příponou .scss
- **assets** - aktiva 
    - **image** - obrázky
        - **resize** (*generováno*) - vygenerované náhledy obrázků
- **js** - javascript soubory (při používání ES6 bude výsledek převeden pomocí knihovny Babel do verze ES5)
- **modules** (*generováno* z node_modules) - knihovny třetích stran
    - **iframemanager** - nemá npm zdroj
- HTML soubory s příponou .html
- EJS soubory s podtržítkem na začátku a příponou .ejs (opakující se části HTML kódu)

## Stažení

```bash
git clone git@gitlab.com:vavyskov/web-gulp.git
```

Případně stažení **ZIP** archivu.


## Instalace

1. Nainstalujte [Node.js](https://nodejs.org/en/) - obsahuje správce balíčků **npm**.
	- Verze:
	  - **Current** (ve Windows včetně **Tools**)
	  - **LTS** (ve Windows spusťte terminál jako správce a doinstalujte **Tools** `npm install --global windows-build-tools`) 
	- Test funkčnosti: `npm --version`
1. Nainstalujte [ImageMagick](http://www.imagemagick.org/script/index.php) **včetně nástroje convert** pro práci s obrázky v příkazovém řádku (vyžaduje **znovu přihlášení** uživatele, případně **restart** počítače).
	- Test funkčnosti: `convert --version`
1. Přejděte do adresáře **web-gulp/**.
1. Nainstalujte požadované *npm* balíčky a závislosti příkazem:

```bash
npm install
npm link gulp
```

## Vývoj

### Spuštění režimu lokálního vývoje

```bash
npm run watch
```

- vygenerování výsledného webu:
  - výsledné soubory HTML, CSS a JS nejsou komprimovány
  - pro obrázky je použita rychlá komprese
- vygenerování dokumentace z anotačních komentářů
- sledování změn souborů a jejich synchronizace s prohlížeči

### Vygenerování výsledného webu

```bash
npm run build
```

- vygenerování výsledného webu:
  - výsledné soubory HTML, CSS a JS jsou zkomprimovány
  - pro obrázky je použita maximální komprese
- vygenerování dokumentace z anotačních komentářů

### Vygenerování dokumentace

```bash
npm run docs
```

- pouze vygenerování dokumentace z anotačních komentářů

---

### Samostatně spustitelné úlohy

Samostatně spustitelné úlohy definované v souboru `gulpfile.js`:

```bash
gulp build
gulp watch
gulp watchOnly
gulp clean
gulp html
gulp html --mode development
gulp css
gulp css --mode development
gulp js
gulp js --mode development
gulp resizeImage
gulp img
gulp img --mode development
gulp root
gulp assets
gulp modules
```

## FixMe

- IE11: scroll progress bar
