// Packages
import gulp from 'gulp';
import del from 'del';
import mergeStream from 'merge-stream';
import htmlmin from 'gulp-htmlmin';
import ejs from 'gulp-ejs';
import gulpif from 'gulp-if';

// import babel from 'gulp-babel';
import uglify from 'gulp-uglify';
import browserify from 'browserify';
import babelify from 'babelify';
import notify from 'gulp-notify';
import source from 'vinyl-source-stream';
import streamify from 'gulp-streamify';

import imageResize from 'gulp-image-resize';
import rename from 'gulp-rename';
import imagemin from 'gulp-imagemin'; // Quick development
import image from 'gulp-image'; // Pruduction - better compression than gulp-imagemin!!!

import yargs from 'yargs';
const argv = yargs.argv;

import autoprefixer from 'gulp-autoprefixer';
import cleanCss from 'gulp-clean-css';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);

import browserSync from 'browser-sync';
browserSync.create();

// Development mode
let minify = true;
if (argv.mode === 'development') {
    minify = false;
}

// Clean (Delete folders)
export function clean() {
    return del([
        'public',
        'src/assets/image/resize/**/*',
        '!src/assets/image/resize/README.md',
        'src/modules/**/*',
        '!src/modules/README.md',
        '!src/modules/iframemanager',
    ]);
}

// HTML (EJS template, Minify)
export function html() {
    return gulp.src('src/*.html')
        .pipe(ejs({}, {}, { ext: '.html' }))
        .pipe(gulpif(minify, htmlmin({
            // removeOptionalTags: true, // Note: BrowserSync works only if page has a body tag
            collapseWhitespace: true,
            removeComments: true,
            minifyJS: true,
            minifyCSS: true,
        })))
        .pipe(gulp.dest('public'))
        .pipe(browserSync.stream());
}

// CSS (SCSS, Minfy)
export function css() {
    return gulp.src('src/css/*.{css,scss}')
        .pipe(sass({
            outputStyle: 'expanded',
            // includePaths: "./node_modules",
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false,
            grid: 'autoplace',
        }))
        .pipe(gulpif(minify, cleanCss()))
        /* .pipe(cleanCss({
            level: {
                1: {
                    //specialComments: false
                }
            }
        })) */
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.stream());
}

// JS (Babel, Polyfill, Minify) - only one file
export function js() {
    return browserify('src/js/script.js')
        .transform(babelify, {
            presets: [
                '@babel/preset-env',
            ],
            plugins: [
                '@babel/plugin-transform-runtime',
                '@babel/plugin-proposal-object-rest-spread',
            ],
            sourceMapsAbsolute: true,
        })
        .bundle()
        .on('error', notify.onError((error) => `JS: ${error.message}`))
        .pipe(source('script.js'))
        //.pipe(gulpif(minify, uglify()))
        .pipe(streamify(gulpif(minify, uglify())))
        .pipe(gulp.dest('public/js'))
        .pipe(browserSync.stream())
    ;
}

// JS (Babel, Minify) - multiple files (ToDo: Polyfill)
/* export function js() {
    return gulp.src([
            'src/js/*.js',
            '!src/js/_*'
        ])
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulpif(minify, uglify()))
        .pipe(gulp.dest('public/js'))
        .pipe(browserSync.stream())
    ;
} */


// Modules (Copy modules files)
export function modules() {
    const sanitizeCss = gulp.src('node_modules/sanitize.css/**')
        .pipe(gulp.dest('src/modules/sanitize.css'))
        .pipe(gulp.dest('public/modules/sanitize.css'));
    const jquery = gulp.src([
        'node_modules/jquery/dist/*',
        '!node_modules/jquery/dist/core.js',
    ])
        .pipe(gulp.dest('src/modules/jquery'))
        .pipe(gulp.dest('public/modules/jquery'));
    const popper = gulp.src('node_modules/popper.js/dist/**/*')
        .pipe(gulp.dest('src/modules/popper.js'))
        .pipe(gulp.dest('public/modules/popper.js'));
    const bootstrap = gulp.src('node_modules/bootstrap/dist/**/*')
        .pipe(gulp.dest('src/modules/bootstrap'))
        .pipe(gulp.dest('public/modules/bootstrap'));
    const footable = gulp.src([
        'node_modules/footable/css/**/*',
        'node_modules/footable/dist/**/*',
    ])
        .pipe(gulp.dest('src/modules/footable'))
        .pipe(gulp.dest('public/modules/footable'));
    const fontAwesome = gulp.src([
        'node_modules/@fortawesome/fontawesome-free/**/*',
        '!node_modules/@fortawesome/fontawesome-free/less{,/**}',
        '!node_modules/@fortawesome/fontawesome-free/scss{,/**}',
        '!node_modules/@fortawesome/fontawesome-free/package.json',
    ])
        .pipe(gulp.dest('src/modules/fontawesome-free'))
        .pipe(gulp.dest('public/modules/fontawesome-free'));
    const slickCarousel = gulp.src('node_modules/slick-carousel/slick/**/*')
        .pipe(gulp.dest('src/modules/slick-carousel'))
        .pipe(gulp.dest('public/modules/slick-carousel'));
    const ckeditor = gulp.src('node_modules/ckeditor4/**/*')
        .pipe(gulp.dest('src/modules/ckeditor4'))
        .pipe(gulp.dest('public/modules/ckeditor4'));
    const flipdown = gulp.src('node_modules/flipdown/dist/**/*')
        .pipe(gulp.dest('src/modules/flipdown'))
        .pipe(gulp.dest('public/modules/flipdown'));
    const vanillaCookieconsent = gulp.src('node_modules/vanilla-cookieconsent/dist/**/*')
        .pipe(gulp.dest('src/modules/vanilla-cookieconsent'))
        .pipe(gulp.dest('public/modules/vanilla-cookieconsent'));

    // The iframemanager does not have an npm source :(
    const iframemanager = gulp.src('src/modules/iframemanager/**/*')
        .pipe(gulp.dest('public/modules/iframemanager'));

    return mergeStream(
        sanitizeCss,
        jquery,
        popper,
        bootstrap,
        footable,
        fontAwesome,
        slickCarousel,
        ckeditor,
        flipdown,
        vanillaCookieconsent,
        iframemanager,
    );
}

// Assets (Copy assets files)
export function assets() {
    const other = gulp.src([
        'src/assets/**/*.*',
        '!src/assets/image{,/**}',
    ])
        .pipe(gulp.dest('public/assets'));
    const images = gulp.src('src/assets/image/**/*.*')
        .pipe(gulp.dest('public/assets/image'));
    return mergeStream(other, images);
}

// Resize (Generate thumbnail - Require ImageMagick "convert")
export function resizeImage() {
    const resizeSm = gulp.src('src/assets/image/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 540,
            imageMagick: true,
        }))
        .pipe(rename({
            suffix: '-sm',
        }))
        .pipe(gulp.dest('src/assets/image/resize'));
    const resizeMd = gulp.src('src/assets/image/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 720,
            imageMagick: true,
        }))
        .pipe(rename({
            suffix: '-md',
        }))
        .pipe(gulp.dest('src/assets/image/resize'));
    const resizeLg = gulp.src('src/assets/image/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 960,
            imageMagick: true,
        }))
        .pipe(rename({
            suffix: '-lg',
        }))
        .pipe(gulp.dest('src/assets/image/resize'));
    const resizeXl = gulp.src('src/assets/image/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 1140,
            imageMagick: true,
        }))
        .pipe(rename({
            suffix: '-xl',
        }))
        .pipe(gulp.dest('src/assets/image/resize'));
    return mergeStream(resizeSm, resizeMd, resizeLg, resizeXl);
}

// Image (Optimise PNG, JPG, GIF nad SVG images)
export function img() {
    return gulp.src('src/assets/image/**/*.{jpg,png,gif,svg}')
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/image'));
}

/**
 * Copy root files
 */
export function root() {
    return gulp.src('src/{robots.txt,sitemap.xml}')
        .pipe(gulp.dest('public'));
}
// Fixme: Correct robots.txt and sitemap.xml content


// BrowserSync
function serve(done) {
    browserSync.init({
        server: {
            baseDir: 'public',
        },
        port: 3000,
        notify: false,
    });
    done();
}

// Watch files
function watchFiles() {
    gulp.watch('src/*.{html,ejs}', html);
    gulp.watch('src/css/*.{css,scss}', css);
    gulp.watch('src/js/*.js', js);
}

// Define complex tasks
// const build = gulp.series(clean, gulp.parallel(modules, html, css, js, assets, root));
export const build = gulp.series(
    clean,
    gulp.parallel(
        modules,
        html,
        css,
        js,
        assets,
        root,
        gulp.series(resizeImage, img),
    ),
);
export const watch = gulp.series(build, gulp.parallel(watchFiles, serve));
export const watchOnly = gulp.parallel(watchFiles, serve);

// Export tasks
export default build;
